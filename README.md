# britannia services

This projects is an API that serve:

- members API
- comments API 

## Documentations and Endpoints

- Member API
  Please refer to the [member/docs/openapi.yaml](/member/docs/openapi.yaml) with [Swagger Editor](https://editor.swagger.io/).

- Comment API
  Please refer to the [comment/docs/openapi.yaml](/comment/docs/openapi.yaml) with [Swagger Editor](https://editor.swagger.io/).

## Development

##### Prerequisite
 - Go 1.12 (support go module)
 - Unix-family Workspace (Linux or MacOS) *the application developed under MacOS.

##### Checkout

First, clone the application to any directory as long as not in GOPATH (since this project use Go Module, so clonning in GOPATH is not recommended)

```bash
$ git clone git@bitbucket.org:bxcodec/britannia.git your_target_directory

$ cd your_target_directory/britannia
```

## Running All the Application Services

Follow this steps to run the applications locally.

##### Dockerize

```bash
$ make docker
```

##### Configurations File
Please create a `member_config.toml`  and `comment_config.toml` file in the root project. The configuration example can be looked in [comment.config.toml.example](/comment_config.toml.example) and  [member.config.toml.example](/member_config.toml.example).
Or you can just copy this to run locally.

*member_config.toml*
```toml
debug=true
address=":9090"
contextTimeout=2 #in second
[mongo]
    uri="mongodb://member_mongo:27017"
    dbname="member"
```

*comment_config.toml*
```
debug=true
address=":9090"
contextTimeout=2 #in second
[mongo]
    uri="mongodb://comment_mongo:27017"
    dbname="comment"
```

##### Run the services

```
$ make run 
```

##### Install Migrations Tools and Run the Migration
The migrations only available for member services, since this service don't have any insert endpoint. So to simulate the Fetch functions, I create a migrations file.
For now, I can't make any action in Makefile to do this automatically. Still don't figure out how to do this in Makefile action.

```bash
$ cd member
$ make migrate-prepare
$ cd ../
$ make migrate-up
```

After run the migrations, the applications should be ready to use at `http://localhost:9191/orgs/{organization}/members` for member service and  `http://localhost:9191/orgs/{organization}/comments` for comment service

```
$ curl -v http://localhost:9191/orgs/xendit/members
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9090 (#0)
> GET /orgs/xendit/members HTTP/1.1
> Host: localhost:9090
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=UTF-8
< X-Cursor: MQ==
< Date: Sat, 18 May 2019 18:08:29 GMT
< Content-Length: 270
<
[{"id":"5cdfd8fbda8b568ad1f06814","username":"bxcodec","avatar":"https://github.com/bxcodec.png","follower":1400,"following":1000},{"id":"5cdfd8fbda8b568ad1f06815","username":"imantumorang","avatar":"https://github.com/imantumorang.png","follower":14,"following":1000}]
* Connection #0 to host localhost left intact

$ curl -v http://localhost:9191/orgs/xendit/comments
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9191 (#0)
> GET /orgs/xendit/comments HTTP/1.1
> Host: localhost:9191
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Server: nginx/1.15.9
< Date: Sat, 18 May 2019 18:53:04 GMT
< Content-Type: application/json; charset=UTF-8
< Content-Length: 109
< Connection: keep-alive
< X-Cursor: NWNlMDRlYWM1ODVjZDQ5ZDYxNWYwMmM1
<
[{"id":"5ce04eac585cd49d615f02c5","comment":"Xendit is Hiring!!!","created_time":"2019-05-18T18:27:56.37Z"}]
* Connection #0 to host localhost left intact
```