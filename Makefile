.PHONY: docker-reverse-proxy
docker-reverse-proxy:
	@docker build -f reverse-proxy/Dockerfile -t reverse-proxy ./reverse-proxy

.PHONY: docker-member
docker-member:
	@make -C ./member docker

.PHONY: docker-comment
docker-comment:
	@make -C ./comment docker

.PHONY: docker
docker: docker-reverse-proxy docker-member docker-comment

.PHONY: run
run: docker
	@make -C ./comment stop
	@make -C ./member stop
	@docker-compose up -d 

.PHONY: stop
stop:
	@docker-compose down

.PHONY: migrate-up
migrate-up:
	@make -C ./member migrate-up

.PHONY: migrate-down
migrate-down:
	@make -C ./member migrate-down
