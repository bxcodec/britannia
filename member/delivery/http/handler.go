package http

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/bxcodec/britannia/member"
	echo "github.com/labstack/echo/v4"
)

type memberHandler struct {
	service member.Service
}

// InitMemberDelivery will initialize the member delivery layer
func InitMemberDelivery(e *echo.Echo, service member.Service) {
	handler := &memberHandler{
		service: service,
	}

	e.GET("/orgs/:id/members", handler.Fetch)
}

func (h *memberHandler) Fetch(c echo.Context) (err error) {
	limitStr := c.QueryParam("limit")
	limit := 20 //default
	if limitStr != "" {
		limit, err = strconv.Atoi(limitStr)
		if err != nil {
			return c.JSON(http.StatusBadRequest, fmt.Errorf("Limit param is invalid: %s", err.Error()))
		}
	}
	cursor := c.QueryParam("cursor")
	organizationID := c.Param("id")
	qparam := member.QueryParam{
		Limit:          limit,
		Cursor:         cursor,
		OrganizationID: organizationID,
	}

	ctx := c.Request().Context()

	res, nextCursor, err := h.service.Fetch(ctx, qparam)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, res)
	}

	c.Response().Header().Set("X-Cursor", nextCursor)
	return c.JSON(http.StatusOK, res)
}
