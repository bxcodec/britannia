package http_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/bxcodec/britannia/member"
	handler "bitbucket.org/bxcodec/britannia/member/delivery/http"
	"bitbucket.org/bxcodec/britannia/member/mocks"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestFetch(t *testing.T) {
	e := echo.New()
	mockArrMember := []member.Member{
		member.Member{
			ID:        "id-user-bxcodec",
			Username:  "bxcodec",
			Avatar:    "https://github.com/bxcodec.png",
			Follower:  140,
			Following: 1000,
		},
		member.Member{
			ID:        "id-user-eminartiys",
			Username:  "eminartiys",
			Avatar:    "https://github.com/eminartiys.png",
			Follower:  150,
			Following: 100,
		},
	}

	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/orgs/xendit/members?limit=1", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Fetch", mock.Anything, member.QueryParam{Limit: 1, OrganizationID: "xendit"}).Return(mockArrMember, "next-cursor", nil).Once()

		handler.InitMemberDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusOK, rec.Code)
		require.Equal(t, "next-cursor", rec.Header().Get("X-Cursor"))
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/orgs/xendit/members?limit=1", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Fetch", mock.Anything, member.QueryParam{Limit: 1, OrganizationID: "xendit"}).Return(nil, "", errors.New("Internl Server Error")).Once()

		handler.InitMemberDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})

}
