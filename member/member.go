package member

import "context"

// Member ...
type Member struct {
	ID           string `json:"id" bson:"_id"`
	Username     string `json:"username" bson:"username"`
	Avatar       string `json:"avatar" bson:"avatar"`
	Follower     int64  `json:"follower" bson:"follower"`
	Following    int64  `json:"following" bson:"following"`
	Organization string `json:"-" bson:"organization"`
}

// QueryParam ...
type QueryParam struct {
	Limit          int
	Cursor         string
	OrganizationID string
}

// Service ...
type Service interface {
	Fetch(ctx context.Context, queryParam QueryParam) (res []Member, nextCursor string, err error)
}

// Repository ...
type Repository interface {
	Fetch(ctx context.Context, queryParam QueryParam) (res []Member, nextCursor string, err error)
}
