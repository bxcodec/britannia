package member

import "errors"

var (
	// ErrBadRequestParam represent that the given params is not valid
	ErrBadRequestParam = errors.New("Bad Request Param")
)
