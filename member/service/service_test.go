package service_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"bitbucket.org/bxcodec/britannia/member"
	"bitbucket.org/bxcodec/britannia/member/mocks"
	"bitbucket.org/bxcodec/britannia/member/service"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestFetch(t *testing.T) {
	mockArrMember := []member.Member{
		member.Member{
			ID:        "id-user-bxcodec",
			Username:  "bxcodec",
			Avatar:    "https://github.com/bxcodec.png",
			Follower:  140,
			Following: 1000,
		},
		member.Member{
			ID:        "id-user-eminartiys",
			Username:  "eminartiys",
			Avatar:    "https://github.com/eminartiys.png",
			Follower:  150,
			Following: 100,
		},
	}

	testCases := []struct {
		desc        string
		repo        func() *mocks.Repository
		params      member.QueryParam
		expectedRes []interface{}
	}{
		{
			desc: "success",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Fetch", mock.Anything, member.QueryParam{Limit: 2, OrganizationID: "xendit"}).Return(mockArrMember, "next-cursor", nil)
				return
			},
			params:      member.QueryParam{Limit: 2, OrganizationID: "xendit"},
			expectedRes: []interface{}{mockArrMember, "next-cursor", nil},
		},
		{
			desc: "error from repository",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Fetch", mock.Anything, member.QueryParam{Limit: 2, OrganizationID: "xendit"}).Return([]member.Member{}, "", errors.New("Unknown Error"))
				return
			},
			params:      member.QueryParam{Limit: 2, OrganizationID: "xendit"},
			expectedRes: []interface{}{[]member.Member{}, "", errors.New("Unknown Error")},
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			serv := service.New(tC.repo(), time.Second*2)
			res, csr, err := serv.Fetch(context.Background(), tC.params)
			require.Equal(t, tC.expectedRes[0], res)
			require.Equal(t, tC.expectedRes[1], csr)
			require.Equal(t, tC.expectedRes[2], err)
		})
	}
}
