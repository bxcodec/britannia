package service

import (
	"context"
	"time"

	"bitbucket.org/bxcodec/britannia/member"
)

type service struct {
	timeout time.Duration
	repo    member.Repository
}

// New will return the implementation of member service
func New(repo member.Repository, timeout time.Duration) member.Service {
	return &service{
		repo:    repo,
		timeout: timeout,
	}
}

func (s *service) Fetch(ctx context.Context, queryParam member.QueryParam) (res []member.Member, nextCursor string, err error) {
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()

	res, nextCursor, err = s.repo.Fetch(ctx, queryParam)
	return
}
