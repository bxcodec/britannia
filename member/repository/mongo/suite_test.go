package mongo_test

import (
	"context"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoSuite struct {
	suite.Suite
	Database *mongo.Database
	Client   *mongo.Client
}

func (s *mongoSuite) SetupSuite() {
	mongoDSN := os.Getenv("MONGO_TEST_URI")
	if mongoDSN == "" {
		mongoDSN = "mongodb://localhost:27019"
	}

	dbName := os.Getenv("MONGO_TEST_DB")
	if dbName == "" {
		dbName = "testDB"
	}

	opts := options.Client().
		SetConnectTimeout(2 * time.Second).
		SetServerSelectionTimeout(3 * time.Second).
		ApplyURI(mongoDSN)

	client, err := mongo.Connect(context.Background(), opts)
	require.NoError(s.T(), err)

	err = client.Ping(context.Background(), nil)
	require.NoError(s.T(), err)

	db := client.Database(dbName)
	s.Database = db
	s.Client = client
}

func (s *mongoSuite) TearDownSuite() {
	err := s.Client.Disconnect(context.Background())
	if err != nil {
		logrus.Error(err)
	}
}
