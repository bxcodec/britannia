package mongo_test

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/bxcodec/britannia/member"
	mongoRepo "bitbucket.org/bxcodec/britannia/member/repository/mongo"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo"
)

type memberMongoSuite struct {
	mongoSuite
}

func TestMemberMongoSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip member repository test")
	}

	suite.Run(t, new(memberMongoSuite))
}

func (s *memberMongoSuite) TearDownTest() {
	// coll := s.Database.Collection(mongoRepo.CollectionMember)
	// err := coll.Drop(context.Background())
	// require.NoError(s.T(), err)
}

func (s *memberMongoSuite) seed() {
	coll := s.Database.Collection(mongoRepo.CollectionMember)
	mockArrMember := []interface{}{
		member.Member{
			ID:           primitive.NewObjectID().Hex(),
			Username:     "bxcodec",
			Avatar:       "https://github.com/bxcodec.png",
			Follower:     1400,
			Following:    1000,
			Organization: "xendit",
		},
		member.Member{
			ID:           primitive.NewObjectID().Hex(),
			Username:     "imantumorang",
			Avatar:       "https://github.com/imantumorang.png",
			Follower:     14,
			Following:    1000,
			Organization: "xendit",
		},
		member.Member{
			ID:           primitive.NewObjectID().Hex(),
			Username:     "johndoe",
			Avatar:       "https://github.com/johndoe.png",
			Follower:     150,
			Following:    100,
			Organization: "xendit",
		},
		member.Member{
			ID:           primitive.NewObjectID().Hex(),
			Username:     "johnsnow",
			Avatar:       "https://github.com/johnsnow.png",
			Follower:     1500,
			Following:    100,
			Organization: "xendit",
		},
	}

	res, err := coll.InsertMany(context.Background(), mockArrMember)
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(mockArrMember), len(res.InsertedIDs))
}

func (s *memberMongoSuite) TestFetch() {
	s.seed()
	repo := mongoRepo.New((s.Database))

	s.T().Run("success", func(t *testing.T) {
		qparam := member.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
		}
		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.NotEmpty(s.T(), nextCursor)
		require.Len(s.T(), res, 1)

		fmt.Println("Next Cursor>> ", nextCursor)

		require.Equal(s.T(), mongoRepo.EncodeCursor("1"), nextCursor)
		require.Equal(s.T(), "johnsnow", res[0].Username)
	})

	s.T().Run("success with cursor", func(t *testing.T) {
		qparam := member.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
			Cursor:         mongoRepo.EncodeCursor("1"), // using cursor the first page
		}

		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.NotEmpty(s.T(), nextCursor)
		require.Len(s.T(), res, 1)

		require.Equal(s.T(), mongoRepo.EncodeCursor("2"), nextCursor)
		require.Equal(s.T(), "bxcodec", res[0].Username)
	})

	s.T().Run("end of page", func(t *testing.T) {
		qparam := member.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
			Cursor:         mongoRepo.EncodeCursor("5"), // using the latest cursor
		}
		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.Empty(s.T(), nextCursor)
		require.Len(s.T(), res, 0)
	})
}
