package mongo

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"bitbucket.org/bxcodec/britannia/member"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type memberRepository struct {
	DB *mongo.Database
}

const (
	// CollectionMember represent the name of member collection
	CollectionMember = "member"
)

// New will return the implementation of member repository
func New(db *mongo.Database) member.Repository {
	return &memberRepository{
		DB: db,
	}
}

func (r *memberRepository) Fetch(ctx context.Context, queryParam member.QueryParam) (res []member.Member, nextCursor string, err error) {
	res = []member.Member{}
	filters := bson.M{}
	coll := r.DB.Collection(CollectionMember)
	opts := options.Find().SetProjection(
		bson.M{
			"_id":       true,
			"username":  true,
			"avatar":    true,
			"follower":  true,
			"following": true,
		}).SetSort(bson.M{
		"follower": -1,
		"_id":      -1,
	})

	if queryParam.Limit > 0 {
		opts = opts.SetLimit(int64(queryParam.Limit))
	}

	if queryParam.OrganizationID != "" {
		filters["organization"] = queryParam.OrganizationID
	}
	skip := int64(0)
	if queryParam.Cursor != "" {
		csr, er := DecodeCursor(queryParam.Cursor)
		if er != nil {
			logrus.Warningf("Error when parsing cursor: %s", er.Error())
			err = member.ErrBadRequestParam
			return
		}
		skip, er = strconv.ParseInt(csr, 10, 64)
		if er != nil {
			logrus.Warningf("Error when parsing cursor: %s", er.Error())
			err = member.ErrBadRequestParam
			return
		}
		opts = opts.SetSkip(skip)
	}

	mongoCursor, err := coll.Find(ctx, filters, opts)
	if err != nil {
		err = errors.Wrap(err, "error when fetching members from mongodb")
		return
	}

	for mongoCursor.Next(ctx) {
		var m member.Member
		er := mongoCursor.Decode(&m)
		if er != nil {
			err = errors.Wrap(er, "error when decoding members from mongodb")
			return
		}
		res = append(res, m)
	}

	if len(res) == 0 {
		return
	}
	skip++
	rawCsr := fmt.Sprintf("%d", skip)
	nextCursor = EncodeCursor(rawCsr)
	return
}

// EncodeCursor will encode cursor to base64 string
func EncodeCursor(objectID string) (res string) {
	return base64.StdEncoding.EncodeToString([]byte(objectID))
}

// DecodeCursor will decode cursor from bas64 string
func DecodeCursor(cursor string) (res string, err error) {
	dst, err := base64.StdEncoding.DecodeString(cursor)
	res = string(dst)
	return
}
