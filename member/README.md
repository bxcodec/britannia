# member API

This package is contains the member API projects. 

## Documentations and Endpoints

Please refer to the [docs/openapi.yaml](/member/docs/openapi.yaml) with [Swagger Editor](https://editor.swagger.io/).

## Development

##### Prerequisite
 - Go 1.12 (support go module)
 - Unix-family Workspace (Linux or MacOS) *the application developed under MacOS.

##### Checkout

First, clone the application to any directory as long as not in GOPATH (since this project use Go Module, so clonning in GOPATH is not recommended)

```bash
$ git clone git@bitbucket.org:bxcodec/britannia.git your_target_directory

$ cd your_target_directory/member
```

## Preparing workspace

##### Install Linter

```bash
$ make lint-prepare
```

##### Apply Linter

```bash
$ make lint
```
 
## Testing
For the testing, there are 2 kind available test included in this projects.

##### Unit Testing

```bash
$ make unittest
```

##### Integration Testing

```bash
$ make test
```

## Running the Application

Follow this steps to run the applications locally.

##### Install Migrations Tools

```bash
$ make migrate-prepare
```

##### Dockerize

```bash
$ make docker
```

##### Prepare the config file
Please create a `config.toml` file in the root project. The configuration example can be looked in [config.toml.example](/member/config.toml.example).
Or you can just copy this to run locally.

```toml
debug=true
address=":9090"
contextTimeout=2 #in second
[mongo]
    uri="mongodb://mongo:27017"
    dbname="member"
```

##### Run the services

```
$ make run 
```

##### Run the migration

```bash
$ make migrate-up
```

After run the migrations, the applications should be ready to use at `http://localhost:9090/orgs/{organization}/members`

```
$ curl -v http://localhost:9090/orgs/xendit/members
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9090 (#0)
> GET /orgs/xendit/members HTTP/1.1
> Host: localhost:9090
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=UTF-8
< X-Cursor: MQ==
< Date: Sat, 18 May 2019 18:08:29 GMT
< Content-Length: 270
<
[{"id":"5cdfd8fbda8b568ad1f06814","username":"bxcodec","avatar":"https://github.com/bxcodec.png","follower":1400,"following":1000},{"id":"5cdfd8fbda8b568ad1f06815","username":"imantumorang","avatar":"https://github.com/imantumorang.png","follower":14,"following":1000}]
* Connection #0 to host localhost left intact
```