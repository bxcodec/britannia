package main

import (
	"context"
	"log"
	"time"

	handler "bitbucket.org/bxcodec/britannia/member/delivery/http"
	mongorepo "bitbucket.org/bxcodec/britannia/member/repository/mongo"
	"bitbucket.org/bxcodec/britannia/member/service"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func init() {
	initConfig()
}

func initConfig() {
	viper.SetConfigType("toml")
	viper.SetConfigFile("config.toml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	logrus.Info("Using Config file: ", viper.ConfigFileUsed())

	if viper.GetBool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Warn("Member service is Running in Debug Mode")
		return
	}
	logrus.SetLevel(logrus.InfoLevel)
	logrus.Warn("Member service is Running in Production Mode")
}

func main() {
	// MONGO DB
	mongoURL := viper.GetString("mongo.uri")
	if mongoURL == "" {
		log.Fatal("Mongo URI is not well-set")
	}

	mongoOptions := options.Client().
		SetConnectTimeout(2 * time.Second).
		SetServerSelectionTimeout(3 * time.Second).
		ApplyURI(mongoURL)

	client, err := mongo.Connect(context.Background(), mongoOptions)
	if err != nil {
		log.Fatal("Mongo connection failed: ", err.Error())
	}
	mongoDBName := viper.GetString("mongo.dbname")
	if mongoDBName == "" {
		log.Fatal("Mongo DB name is not well-set")
	}
	db := client.Database(mongoDBName)

	repo := mongorepo.New(db)
	timeoutCtx := time.Second * time.Duration(viper.GetInt64("contextTimeout"))
	serv := service.New(repo, timeoutCtx)

	e := echo.New()
	e.Use(
		handler.ErrorMiddleware(),
	)
	handler.InitMemberDelivery(e, serv)
	log.Fatal(e.Start(viper.GetString("address")))
}
