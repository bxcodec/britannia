package service_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"bitbucket.org/bxcodec/britannia/comment"
	"bitbucket.org/bxcodec/britannia/comment/mocks"
	"bitbucket.org/bxcodec/britannia/comment/service"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestFetch(t *testing.T) {
	mockArrComment := []comment.Comment{
		comment.Comment{
			ID:           "comment-hire",
			Comment:      "Xendit is hiring for Software Engineer",
			Organization: "xendit",
		},
		comment.Comment{
			ID:           "comment-awesome",
			Comment:      "Xendit is Awesome!!!",
			Organization: "xendit",
		},
	}

	testCases := []struct {
		desc        string
		repo        func() *mocks.Repository
		params      comment.QueryParam
		expectedRes []interface{}
	}{
		{
			desc: "success",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Fetch", mock.Anything, comment.QueryParam{Limit: 2, OrganizationID: "xendit"}).Return(mockArrComment, "next-cursor", nil)
				return
			},
			params:      comment.QueryParam{Limit: 2, OrganizationID: "xendit"},
			expectedRes: []interface{}{mockArrComment, "next-cursor", nil},
		},
		{
			desc: "error from repository",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Fetch", mock.Anything, comment.QueryParam{Limit: 2, OrganizationID: "xendit"}).Return([]comment.Comment{}, "", errors.New("Unknown Error"))
				return
			},
			params:      comment.QueryParam{Limit: 2, OrganizationID: "xendit"},
			expectedRes: []interface{}{[]comment.Comment{}, "", errors.New("Unknown Error")},
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			serv := service.New(tC.repo(), time.Second*2)
			res, csr, err := serv.Fetch(context.Background(), tC.params)
			require.Equal(t, tC.expectedRes[0], res)
			require.Equal(t, tC.expectedRes[1], csr)
			require.Equal(t, tC.expectedRes[2], err)
		})
	}
}

func TestPost(t *testing.T) {
	mockComment := comment.Comment{
		Comment:      "Xendit is hiring for Software Engineer",
		Organization: "xendit",
	}
	mockInvalidComment := comment.Comment{
		Comment:      "Xendit is hiring for Software Engineer",
		Organization: "", //organization is empty == invalid
	}

	testCases := []struct {
		desc        string
		repo        func() *mocks.Repository
		param       comment.Comment
		expectedRes []interface{}
	}{
		{
			desc: "success",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Post", mock.Anything, &mockComment).Return(nil)
				return
			},
			param:       mockComment,
			expectedRes: []interface{}{nil},
		},
		{
			desc: "error from repository",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Post", mock.Anything, &mockComment).Return(errors.New("Unknown Error"))
				return
			},
			param:       mockComment,
			expectedRes: []interface{}{errors.New("Unknown Error")},
		},
		{
			desc: "invalid data",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				return
			},
			param:       mockInvalidComment,
			expectedRes: []interface{}{comment.ErrBadRequestParam},
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			serv := service.New(tC.repo(), time.Second*2)
			err := serv.Post(context.Background(), &tC.param)
			require.Equal(t, tC.expectedRes[0], err)
		})
	}
}

func TestDelete(t *testing.T) {
	testCases := []struct {
		desc        string
		repo        func() *mocks.Repository
		param       string
		expectedRes []interface{}
	}{
		{
			desc: "success",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Delete", mock.Anything, "xendit").Return(nil)
				return
			},
			param:       "xendit",
			expectedRes: []interface{}{nil},
		},
		{
			desc: "error from repository",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				res.On("Delete", mock.Anything, "xendit").Return(errors.New("Unknown Error"))
				return
			},
			param:       "xendit",
			expectedRes: []interface{}{errors.New("Unknown Error")},
		},
		{
			desc: "invalid data",
			repo: func() (res *mocks.Repository) {
				res = new(mocks.Repository)
				return
			},
			param:       "",
			expectedRes: []interface{}{comment.ErrBadRequestParam},
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			serv := service.New(tC.repo(), time.Second*2)
			err := serv.Delete(context.Background(), tC.param)
			require.Equal(t, tC.expectedRes[0], err)
		})
	}
}
