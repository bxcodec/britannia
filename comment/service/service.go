package service

import (
	"context"
	"strings"
	"time"

	"bitbucket.org/bxcodec/britannia/comment"
	validator "gopkg.in/go-playground/validator.v9"
)

type service struct {
	repo    comment.Repository
	timeout time.Duration
}

// New will create an implementation of the comment.Service interface
func New(repo comment.Repository, timeout time.Duration) comment.Service {
	return &service{
		repo:    repo,
		timeout: timeout,
	}
}

func (s *service) Fetch(ctx context.Context, qparam comment.QueryParam) (res []comment.Comment, nextCsr string, err error) {
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()
	qparam.OrganizationID = strings.ToLower(qparam.OrganizationID)
	res, nextCsr, err = s.repo.Fetch(ctx, qparam)
	return
}

func (s *service) Post(ctx context.Context, c *comment.Comment) (err error) {
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()

	valid := validator.New()
	err = valid.Struct(c)
	if err != nil {
		err = comment.ErrBadRequestParam
		return
	}

	c.Organization = strings.ToLower(c.Organization)
	err = s.repo.Post(ctx, c)
	return
}
func (s *service) Delete(ctx context.Context, organizationID string) (err error) {
	ctx, cancel := context.WithTimeout(ctx, s.timeout)
	defer cancel()

	if organizationID == "" {
		return comment.ErrBadRequestParam
	}
	organizationID = strings.ToLower(organizationID)
	err = s.repo.Delete(ctx, organizationID)
	return
}
