package comment

import (
	"context"
	"time"
)

// Comment represent the comment data struct
type Comment struct {
	ID           string    `json:"id" bson:"_id" `
	Comment      string    `json:"comment" bson:"comment" validate:"required"`
	Organization string    `json:"-" bson:"organization" validate:"required"`
	CreatedTime  time.Time `json:"created_time" bson:"created_time"`
}

// QueryParam represent the available query param for comment project
type QueryParam struct {
	Limit          int
	Cursor         string
	OrganizationID string
}

// Service represente the comment service
type Service interface {
	Fetch(ctx context.Context, qparam QueryParam) (res []Comment, nextCsr string, err error)
	Post(ctx context.Context, c *Comment) (err error)
	Delete(ctx context.Context, organizationID string) (err error)
}

// Repository represent the repository contract
type Repository interface {
	Fetch(ctx context.Context, qparam QueryParam) (res []Comment, nextCsr string, err error)
	Post(ctx context.Context, c *Comment) (err error)
	Delete(ctx context.Context, organizationID string) (err error)
}
