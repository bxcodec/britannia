package http

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/bxcodec/britannia/comment"
	"github.com/labstack/echo/v4"
)

type commentHandler struct {
	service comment.Service
}

// InitCommentDelivery will initialize the comments API handler
func InitCommentDelivery(e *echo.Echo, service comment.Service) {
	handler := &commentHandler{
		service: service,
	}
	e.GET("/orgs/:id/comments", handler.Fetch)
	e.POST("/orgs/:id/comments", handler.Post)
	e.DELETE("/orgs/:id/comments", handler.Delete)
}

func (h *commentHandler) Fetch(c echo.Context) (err error) {
	organizationID := c.Param("id")
	cursor := c.QueryParam("cursor")
	limitStr := c.QueryParam("limit")
	limit := 20 // default
	if limitStr != "" {
		limit, err = strconv.Atoi(limitStr)
		if err != nil {
			return c.JSON(http.StatusBadRequest, fmt.Errorf("Limit param is invalid: %s", err.Error()))
		}
	}

	qparam := comment.QueryParam{
		OrganizationID: organizationID,
		Limit:          limit,
		Cursor:         cursor,
	}
	ctx := c.Request().Context()
	res, nextCursor, err := h.service.Fetch(ctx, qparam)
	if err != nil {
		return err
	}

	c.Response().Header().Set("X-Cursor", nextCursor)
	return c.JSON(http.StatusOK, res)
}

func (h *commentHandler) Post(c echo.Context) (err error) {
	organizationID := c.Param("id")
	var cmt comment.Comment
	err = c.Bind(&cmt)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, fmt.Errorf("Cannot parse the request body %s", err.Error()))
	}
	cmt.Organization = organizationID

	ctx := c.Request().Context()
	err = h.service.Post(ctx, &cmt)
	if err != nil {
		return
	}

	return c.JSON(http.StatusCreated, cmt)
}

func (h *commentHandler) Delete(c echo.Context) (err error) {
	organizationID := c.Param("id")

	ctx := c.Request().Context()
	err = h.service.Delete(ctx, organizationID)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
