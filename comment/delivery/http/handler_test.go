package http_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/bxcodec/britannia/comment"
	handler "bitbucket.org/bxcodec/britannia/comment/delivery/http"
	"bitbucket.org/bxcodec/britannia/comment/mocks"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestFetch(t *testing.T) {
	e := echo.New()
	mockArrComment := []comment.Comment{
		comment.Comment{
			ID:           "comment-hire",
			Comment:      "Xendit is hiring for Software Engineer",
			Organization: "xendit",
		},
		comment.Comment{
			ID:           "comment-awesome",
			Comment:      "Xendit is Awesome!!!",
			Organization: "xendit",
		},
	}
	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/orgs/xendit/comments?limit=1", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Fetch", mock.Anything, comment.QueryParam{Limit: 1, OrganizationID: "xendit"}).Return(mockArrComment, "next-cursor", nil).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusOK, rec.Code)
		require.Equal(t, "next-cursor", rec.Header().Get("X-Cursor"))
		mockService.AssertExpectations(t)
	})

	t.Run("bad-params", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/orgs/xendit/comments?limit=xxxx", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusBadRequest, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("GET", "/orgs/xendit/comments?limit=1", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Fetch", mock.Anything, comment.QueryParam{Limit: 1, OrganizationID: "xendit"}).Return(nil, "", errors.New("Internl Server Error")).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})
}

func TestPost(t *testing.T) {
	e := echo.New()
	mockReqComment := `{
		"comment": "Xendit is Awesome!!!"
	  }`
	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/orgs/xendit/comments", strings.NewReader(mockReqComment))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Post", mock.Anything, &comment.Comment{Organization: "xendit", Comment: "Xendit is Awesome!!!"}).Return(nil).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusCreated, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("unprocessable entity", func(t *testing.T) {
		mockReqComment := `invalid request body`
		req := httptest.NewRequest("POST", "/orgs/xendit/comments", strings.NewReader(mockReqComment))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusUnprocessableEntity, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/orgs/xendit/comments", strings.NewReader(mockReqComment))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Post", mock.Anything, &comment.Comment{Organization: "xendit", Comment: "Xendit is Awesome!!!"}).Return(errors.New("Unknown Error")).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})
}

func TestDelete(t *testing.T) {
	e := echo.New()

	t.Run("success", func(t *testing.T) {
		req := httptest.NewRequest("DELETE", "/orgs/xendit/comments", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Delete", mock.Anything, "xendit").Return(nil).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusNoContent, rec.Code)
		mockService.AssertExpectations(t)
	})

	t.Run("internal server error", func(t *testing.T) {
		req := httptest.NewRequest("DELETE", "/orgs/xendit/comments", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)

		rec := httptest.NewRecorder()

		mockService := new(mocks.Service)
		mockService.On("Delete", mock.Anything, "xendit").Return(errors.New("Unknown Error")).Once()

		handler.InitCommentDelivery(e, mockService)

		e.ServeHTTP(rec, req)
		require.Equal(t, http.StatusInternalServerError, rec.Code)
		mockService.AssertExpectations(t)
	})
}
