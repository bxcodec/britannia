package mongo_test

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/bxcodec/britannia/comment"
	mongoRepo "bitbucket.org/bxcodec/britannia/comment/repository/mongo"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type commentMongoSuite struct {
	mongoSuite
}

func TestCommentSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip comment repository test")
	}

	suite.Run(t, new(commentMongoSuite))
}

func (s *commentMongoSuite) TearDownTest() {
	coll := s.Database.Collection(mongoRepo.CommentCollection)
	err := coll.Drop(context.Background())
	require.NoError(s.T(), err)
}

func (s *commentMongoSuite) seed() (lastIDs []string) {
	coll := s.Database.Collection(mongoRepo.CommentCollection)
	mockArrComment := []interface{}{
		comment.Comment{
			ID:           primitive.NewObjectID().Hex(),
			Comment:      "Xendit is Hiring for Software Engineer",
			Organization: "xendit",
			CreatedTime:  time.Now(),
		},
		comment.Comment{
			ID:           primitive.NewObjectID().Hex(),
			Comment:      "Xendit is Awesome!!!",
			Organization: "xendit",
			CreatedTime:  time.Now(),
		},
		comment.Comment{
			ID:           primitive.NewObjectID().Hex(),
			Comment:      "Kurio is Media Company",
			Organization: "kurio",
			CreatedTime:  time.Now(),
		},
	}

	res, err := coll.InsertMany(context.Background(), mockArrComment)
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(mockArrComment), len(res.InsertedIDs))

	for _, item := range res.InsertedIDs {
		s := item.(string)
		lastIDs = append(lastIDs, s)
	}
	return lastIDs
}

func (s *commentMongoSuite) TestFetch() {
	insertedIDs := s.seed()
	repo := mongoRepo.New((s.Database))
	s.T().Run("success", func(t *testing.T) {
		qparam := comment.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
		}
		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.NotEmpty(s.T(), nextCursor)
		require.Len(s.T(), res, 1)

		// cursor is shoud be equal to last inserted ids, since the cursor using the id
		require.Equal(s.T(), mongoRepo.EncodeCursor(insertedIDs[1]), nextCursor)
		require.Equal(s.T(), "Xendit is Awesome!!!", res[0].Comment)
	})

	s.T().Run("success with cursor", func(t *testing.T) {
		qparam := comment.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
			Cursor:         mongoRepo.EncodeCursor(insertedIDs[1]), // using cursor the first page (which is the latest item)
		}
		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.NotEmpty(s.T(), nextCursor)
		require.Len(s.T(), res, 1)

		// cursor is shoud be equal to last inserted ids, since the cursor using the id
		require.Equal(s.T(), mongoRepo.EncodeCursor(insertedIDs[0]), nextCursor)
		require.Equal(s.T(), "Xendit is Hiring for Software Engineer", res[0].Comment)
	})

	s.T().Run("end of page", func(t *testing.T) {
		qparam := comment.QueryParam{
			OrganizationID: "xendit",
			Limit:          1,
			Cursor:         mongoRepo.EncodeCursor(insertedIDs[0]), // using cursor the first page (which is the latest item)
		}
		res, nextCursor, err := repo.Fetch(context.Background(), qparam)
		require.NoError(s.T(), err)
		require.Empty(s.T(), nextCursor)
		require.Len(s.T(), res, 0)
	})
}

func (s *commentMongoSuite) TestPost() {
	repo := mongoRepo.New((s.Database))
	mockComment := comment.Comment{
		Comment:      "Xendit is Hiring for Software Engineer",
		Organization: "xendit",
		CreatedTime:  time.Now(),
	}
	require.Empty(s.T(), mockComment.ID)
	err := repo.Post(context.Background(), &mockComment)
	require.NoError(s.T(), err)
	require.NotEmpty(s.T(), mockComment.ID)
}

func (s *commentMongoSuite) TestDelete() {
	s.seed()
	repo := mongoRepo.New((s.Database))

	// Pre-Test State
	qparam := comment.QueryParam{
		OrganizationID: "xendit",
		Limit:          20, // actualy the seeded item is only 2, 20 to fetch all the items
	}
	res, nextCsr, err := repo.Fetch(context.Background(), qparam)
	require.NoError(s.T(), err)
	require.NotEmpty(s.T(), nextCsr)
	require.Len(s.T(), res, 2)

	//Action
	err = repo.Delete(context.Background(), "xendit")
	require.NoError(s.T(), err)

	// Assertion ensure the items is deleted
	qparam = comment.QueryParam{
		OrganizationID: "xendit",
		Limit:          20, // actualy the seeded item is only 2, 20 to fetch all the items
	}
	res, nextCsr, err = repo.Fetch(context.Background(), qparam)
	require.NoError(s.T(), err)
	require.Empty(s.T(), nextCsr)
	require.Len(s.T(), res, 0)

	// Assertion to ensure only xendit organizations that affected
	qparam = comment.QueryParam{
		OrganizationID: "kurio",
		Limit:          20, // actualy the seeded item is only 2, 20 to fetch all the items
	}
	res, nextCsr, err = repo.Fetch(context.Background(), qparam)
	require.NoError(s.T(), err)
	require.NotEmpty(s.T(), nextCsr)
	require.Len(s.T(), res, 1)

	// Assertion ensure the delete methode is "safe delete"
	res = s.findByOrganization("xendit")
	require.Len(s.T(), res, 2)
}

func (s *commentMongoSuite) findByOrganization(orgs string) (res []comment.Comment) {
	coll := s.Database.Collection(mongoRepo.CommentCollection)
	res = []comment.Comment{}
	filters := bson.M{
		"organization": orgs,
	}
	opts := options.Find().SetProjection(
		bson.M{
			"_id":          true,
			"comment":      true,
			"organization": true,
			"created_time": true,
		}).SetSort(bson.M{
		"_id": -1,
	})

	mongoCursor, err := coll.Find(context.Background(), filters, opts)
	require.NoError(s.T(), err)

	for mongoCursor.Next(context.Background()) {
		var m comment.Comment
		err := mongoCursor.Decode(&m)
		require.NoError(s.T(), err)
		res = append(res, m)
	}
	return
}
