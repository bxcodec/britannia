package mongo

import (
	"context"
	"encoding/base64"
	"time"

	"bitbucket.org/bxcodec/britannia/comment"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type commentMongoRepository struct {
	Database *mongo.Database
}

const (
	// CommentCollection represent the comment collection name
	CommentCollection = "comment"
)

// New will return the implementations of Coment repository in Mongo
func New(db *mongo.Database) comment.Repository {
	return &commentMongoRepository{
		Database: db,
	}
}

func (s commentMongoRepository) Fetch(ctx context.Context, qparam comment.QueryParam) (res []comment.Comment, nextCsr string, err error) {
	res = []comment.Comment{}
	filters := bson.M{
		"deleted_time": bson.M{
			"$exists": false,
		},
	}
	coll := s.Database.Collection(CommentCollection)
	opts := options.Find().SetProjection(
		bson.M{
			"_id":          true,
			"comment":      true,
			"organization": true,
			"created_time": true,
		}).SetSort(bson.M{
		"_id": -1,
	})
	if qparam.Limit > 0 {
		opts = opts.SetLimit(int64(qparam.Limit))
	}

	if qparam.OrganizationID != "" {
		filters["organization"] = qparam.OrganizationID
	}

	if qparam.Cursor != "" {
		csr, er := DecodeCursor(qparam.Cursor)
		if er != nil {
			logrus.Warningf("Error when parsing cursor: %s", er.Error())
			err = comment.ErrBadRequestParam
			return
		}

		filters["_id"] = bson.M{"$lt": csr}
	}

	mongoCursor, err := coll.Find(ctx, filters, opts)
	if err != nil {
		err = errors.Wrap(err, "error when fetching comments from mongodb")
		return
	}

	for mongoCursor.Next(ctx) {
		var m comment.Comment
		er := mongoCursor.Decode(&m)
		if er != nil {
			err = errors.Wrap(er, "error when decoding comments from mongodb")
			return
		}
		res = append(res, m)
	}

	if len(res) == 0 {
		return
	}

	nextCsr = EncodeCursor(res[len(res)-1].ID)
	return
}

// EncodeCursor will encode the cursor to base64 string
func EncodeCursor(objectID string) (res string) {
	return base64.StdEncoding.EncodeToString([]byte(objectID))
}

// DecodeCursor will decode  and transform it to internal cursor
func DecodeCursor(cursor string) (res string, err error) {
	dst, err := base64.StdEncoding.DecodeString(cursor)
	res = string(dst)
	return
}

func (s commentMongoRepository) Post(ctx context.Context, c *comment.Comment) (err error) {
	coll := s.Database.Collection(CommentCollection)

	c.CreatedTime = time.Now()
	c.ID = primitive.NewObjectID().Hex()

	res, err := coll.InsertOne(ctx, c)
	if err != nil {
		return
	}
	if c.ID != res.InsertedID.(string) {
		err = errors.New("Invalid inserted ID")
		err = errors.Wrap(err, "error when insert a new comment")
		return
	}
	return
}

func (s commentMongoRepository) Delete(ctx context.Context, organizationID string) (err error) {
	coll := s.Database.Collection(CommentCollection)

	now := time.Now()
	filter := bson.M{
		"organization": organizationID,
	}
	updates := bson.M{
		"$set": bson.M{
			"deleted_time": now,
		},
	}

	res, err := coll.UpdateMany(ctx, filter, updates)
	if err != nil {
		return
	}
	if res.MatchedCount != res.ModifiedCount {
		err = errors.New("Can't delete perfectly, try again")
		err = errors.Wrap(err, "error when deleting comments")
		return
	}

	return
}
