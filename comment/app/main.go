package main

import (
	"context"
	"log"
	"time"

	handler "bitbucket.org/bxcodec/britannia/comment/delivery/http"
	mongorepo "bitbucket.org/bxcodec/britannia/comment/repository/mongo"
	"bitbucket.org/bxcodec/britannia/comment/service"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func init() {
	initConfig()
}

func initConfig() {
	viper.SetConfigType("toml")
	viper.SetConfigFile("config.toml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	logrus.Info("Using Config file: ", viper.ConfigFileUsed())

	if viper.GetBool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Warn("Comment service is Running in Debug Mode")
		return
	}
	logrus.SetLevel(logrus.InfoLevel)
	logrus.Warn("Comment service is Running in Production Mode")
}

func main() {
	// MONGO DB
	mongoURL := viper.GetString("mongo.uri")
	if mongoURL == "" {
		log.Fatal("Mongo URI is not well-set")
	}

	mongoOptions := options.Client().
		SetReadPreference(readpref.PrimaryPreferred()).
		SetConnectTimeout(2 * time.Second).
		SetServerSelectionTimeout(3 * time.Second).
		ApplyURI(mongoURL)

	client, err := mongo.Connect(context.Background(), mongoOptions)
	if err != nil {
		log.Fatal("Mongo connection failed: ", err.Error())
	}

	mongoDBName := viper.GetString("mongo.dbname")
	if mongoDBName == "" {
		log.Fatal("Mongo DB name is not well-set")
	}
	db := client.Database(mongoDBName)

	repo := mongorepo.New(db)
	timeoutCtx := time.Second * time.Duration(viper.GetInt64("contextTimeout"))
	serv := service.New(repo, timeoutCtx)

	e := echo.New()
	e.Use(
		handler.ErrorMiddleware(),
	)
	handler.InitCommentDelivery(e, serv)
	log.Fatal(e.Start(viper.GetString("address")))
}
