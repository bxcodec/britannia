# comment API

This package is contains the comment API projects. 

## Documentations and Endpoints

Please refer to the [docs/openapi.yaml](/comment/docs/openapi.yaml) with [Swagger Editor](https://editor.swagger.io/).

## Development

##### Prerequisite
 - Go 1.12 (support go module)
 - Unix-family Workspace (Linux or MacOS) *the application developed under MacOS.

##### Checkout

First, clone the application to any directory as long as not in GOPATH (since this project use Go Module, so clonning in GOPATH is not recommended)

```bash
$ git clone git@bitbucket.org:bxcodec/britannia.git your_target_directory

$ cd your_target_directory/comment
```

## Preparing workspace

##### Install Linter

```bash
$ make lint-prepare
```

##### Apply Linter

```bash
$ make lint
```
 
## Testing
For the testing, there are 2 kind available test included in this projects.

##### Unit Testing

```bash
$ make unittest
```

##### Integration Testing

```bash
$ make test
```

## Running the Application

Follow this steps to run the applications locally.

##### Dockerize

```bash
$ make docker
```

##### Prepare the config file
Please create a `config.toml` file in the root project. The configuration example can be looked in [config.toml.example](/comment/config.toml.example).
Or you can just copy this to run locally.

```toml
debug=true
address=":9090"
contextTimeout=2 #in second
[mongo]
    uri="mongodb://mongo:27017"
    dbname="comment"
```

##### Run the services

```
$ make run 
```

After run the migrations, the applications should be ready to use at `http://localhost:9090/orgs/{organization}/comments`

```
$ curl -v http://localhost:9090/orgs/xendit/comments
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 9090 (#0)
> GET /orgs/xendit/comments HTTP/1.1
> Host: localhost:9090
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=UTF-8
< X-Cursor: NWNlMDRiMzVkODI2M2U3NDRhZDkxYTQ3
< Date: Sat, 18 May 2019 18:14:01 GMT
< Content-Length: 110
<
[{"id":"5ce04b35d8263e744ad91a47","comment":"Xendit is Hiring!!!","created_time":"2019-05-18T18:13:09.229Z"}]
* Connection #0 to host localhost left intact
```